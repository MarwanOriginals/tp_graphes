import numpy

graphe = [[1, 2], [2], [3], [4], []]



def ini_adjacence(graphe):
  return numpy.full((len(graphe),len(graphe)), 0)

def adjacence(graphe):
    array = ini_adjacence(graphe)
    for i in range(len(graphe)):
        for j in range(len(graphe[i])):
            current_num = graphe[i][j]
            array[i][current_num]+=1
    return array

def fermetureTransitive (matrix):
    result = ""
    length = len(matrix)
    for etape in range(0, length):
        for row in range(0, length):
            for col in range(0, length):
                matrix[row][col] = matrix[row][col] or (matrix[row][etape] and matrix[etape][col])
        result += ("\nT" + str(etape) +"\n" + str(matrix) + "\n")
    result += ("\nLa fermeture transitive est :\n" + str(matrix))
    return result

def P1(adjacence,noeud,visite,voisin):
    visite = visite + [noeud]

    print("je suis sur ",noeud)
    if noeud in range(0,len(adjacence)):
        print(adjacence[noeud])
        for i in range(0,len(adjacence[noeud])):
            if adjacence[noeud][i] == 1:
                print("un fils de ", noeud," est",i)
                voisin = voisin + [i]
        for j in range(len(voisin)):
            if voisin[j] not in  visite:
                        visite = visite + [voisin[j]]
                        print("je suis sur ",noeud," et vais visiter ",voisin[j])
                        P1(adjacence,voisin[j],visite,voisin)
        else:
            print(noeud, " n'a plus de fils")


print(adjacence(graphe))
P1(adjacence(graphe),0,[],[])
#print(fermetureTransitive(adjacence(graphe)))
