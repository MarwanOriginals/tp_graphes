graphe = [[2], [3], [0, 3], [1, 2]]

n = len(graphe)
result = []

for i in range(0, n):
    for j in range(0, len(graphe[i])):
        edge = [i, graphe[i][j]]
        for i2 in range(0, n):
            for j2 in range(0, len(graphe[i2])):
                edge2 = [i2, graphe[i2][j2]]
                if (edge2[0] in edge or edge2[1] in edge) and i != i2:
                    if not [edge2, edge] in result:
                        result.append([edge, edge2])

print(result)

