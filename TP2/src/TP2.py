import numpy

graphe = [[2,3], [2], [3], [4], []]


def mise_En_Niveaux(graphe):
    matriceNiveaux = []
    niveau = 0
    print("Matrice mise a niveau: ")
    while graphe:
        for i in range(len(graphe)):
            if not graphe[i]:
                print("Dernier sommet sans successeur : " + str(i))
                matriceNiveaux.append(("Noeud : " + str(i), "Niveau : " + str(niveau)))
                graphe[i].append("x")
                for x in range(len(graphe)):
                    if i in graphe[x]:
                        graphe[x].remove(i)
                        graphe[x].append("v")
        niveau += 1
        for x in range(len(graphe) - 1, -1, -1):
            if "x" in graphe[x]:
                graphe.pop(x)
        for x in range(len(graphe) - 1, -1, -1):
            if "v" in graphe[x]:
                graphe[x].remove("v")
        print("Résultat au niveau : " + str(niveau))
        for i in range(len(graphe)):
            print(graphe[i])
    print("Résultat final : ")
    for i in range(len(matriceNiveaux)):
        print(matriceNiveaux[i])


mise_En_Niveaux(graphe)
