class Node:
    color = None
    key = None
    colorNames = {None: "NO COLOR", 1: "Rouge", 2: "Bleu", 3: "Vert", 4: "Orange", 5: "Violet"}

    def __init__(self, k):
        self.key = k

    def __str__(self):
        return "(key=" + str(self.key) + ", color=" + str(Node.colorNames[self.color]) + ")"

    def __repr__(self):
        return self.__str__()


class Graph:
    edges = set()
    nodes = []
    nodesFromKeys = {}
    graph = {}
    grundy_Number = 0

    colors = [1, 2, 3, 4, 5, 6]
    _dbg = False

    def __init__(self):
        pass

    def addNode(self, node):
        self.nodes.append(node)
        self.nodesFromKeys[node.key] = node
        self.graph[node.key] = set()

    def addUndirectedEdge(self, N1, N2):
        try:
            self.graph[N1.key].add(N2)
        except:
            self.graph[N1.key] = set([N2])
        try:
            self.graph[N2.key].add(N1)
        except:
            self.graph[N2.key] = set([N1])

    def __str__(self):
        return str(self.graph)

    def fromInput(self):
        n = len(graphe)

        for i in range(0, n):
            key = i
            N = Node(key)
            self.addNode(N)
        for i in range(0, n):
            for j in range(0, len(graphe[i])):
                self.addUndirectedEdge(self.nodesFromKeys[i], self.nodesFromKeys[graphe[i][j]])

    def color(self):
        if self._dbg:
            print("self.nodes=", self.nodes)

        self.nodes = sorted(self.nodes, key=self.getdegree)

        while len(self.nodes) != 0:

            node = self.nodes.pop()
            color = self.findSmallestNotInNeighboors(node)
            if self._dbg:
                print("Chosen color for", node, "is", Node.colorNames[color])
            node.color = color
        return self.nodesFromKeys.values()

    def colorOrder(self):
        if self._dbg:
            print("self.nodes=", self.nodes)

        self.nodes = sorted(self.nodes, key=self.getOrder)
        self.nodes = list(reversed(self.nodes))

        while len(self.nodes) != 0:

            node = self.nodes.pop()
            color = self.findSmallestNotInNeighboors(node)
            if self._dbg:
                print("Chosen color for", node, "is", Node.colorNames[color])
            node.color = color
            if color > self.grundy_Number:
                self.grundy_Number = color
        return self.nodesFromKeys.values()

    def findSmallestNotInNeighboors(self, node):
        c = set(self.colors)
        for n in self.graph[node.key]:
            if self._dbg:
                print("Neighboor:", n)
                print("Colors:", c)
            if n.color is not None:
                try:
                    c.remove(n.color)
                except KeyError:
                    pass  # The color has already been taken out ? Just don't care
        if self._dbg:
            print(c, len(c))
        if 0 == len(c):
            return None
        return c.pop()

    def getdegree(self, node):
        key = node.key
        if self._dbg:
            print("Getting the degree of node", key)
            print("The degree is", len(self.graph[key]))
        return len(self.graph[key])

    def getOrder(self, node):
        for i in range(0, len(order)):
            if str(order[i]) == str(node.key):
                return i + 1

        return 0

graphe = [[1], [2], [3], []]
graphe_order = graphe.copy()
graphe_order.sort()
order = []
for i in range(0, len(graphe_order)):
    for y in range(0, len(graphe)):
        if graphe_order[i] == graphe[y]:
            order.append(y)
G = Graph()
G.fromInput()
print(G.colorOrder())
print("Le nombre de Grundy est : " + str(G.grundy_Number))
